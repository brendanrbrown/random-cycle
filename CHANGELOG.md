# Revision history for random-cycle

## 0.1.2.0 -- 2023-11-19

* Fixes a bug in 'uniformCyclePartitionThin'.

## 0.1.1.0 -- 2023-05-07

* Adds Sattolo's algorithm to sample full cycles.
* Documentation fixes.

## 0.1.0.1 -- 2023-04-18

* Documentation fixes and base package bounds loosened.

## 0.1.0.0 -- 2023-04-17

* First version. Released on an unsuspecting world.
