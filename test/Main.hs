module Main where

import Test.Tasty (defaultMain, testGroup)
import TestCycles
import TestPartitions

main :: IO ()
main = defaultMain $ testGroup "RandomCycle" [testPartitions, testCycles]
