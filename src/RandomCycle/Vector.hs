module RandomCycle.Vector
  ( -- * Partitions
    uniformPartition,
    partitionFromBits,

    -- * Cycles
    uniformCycle,
    uniformCyclePartition,
    uniformCyclePartitionThin,
  )
where

import RandomCycle.Vector.Cycle
import RandomCycle.Vector.Partition
