#!/usr/bin/env bash

# TODO: better as a makefile
set -e

# Save the old for comparison
if [[ -f "benches.prof" ]]; then
	mv benches.prof benches-old.prof
fi
# Not something you want in the CI
cabal clean
cabal build all --enable-profiling
cabal run bench:benches --enable-profiling -- -o benches.html +RTS -p
hp2pretty benches.hp
diff benches-old.prof benches.prof >benches.diff
rm benches-old.prof
