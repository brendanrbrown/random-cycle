;; Commit fixed to state in which ld error when linking Haskell executable 
;; (can't find -lrt) doesn't occur.
(list (channel
       (name 'guix)
       (url "https://git.savannah.gnu.org/git/guix.git")
       (commit "a4e80883bb9777d0f70ec2ddefb14c00b7a799a5")))
